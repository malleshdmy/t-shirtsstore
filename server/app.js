"use strict";

var express = require('express');
var bodyParser = require('body-parser');
var app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
var logger = require('tracer').console({
    format: "{{timestamp}} [{{title}}] {{message}} (in {{path}}:{{line}})",
    dateformat: "dd-mm-yyyy HH:MM:ss TT"
});

var config = require('./config/config.json');

app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));
app.use(function (req, res, next) {

    res.setHeader('Access-Control-Allow-Origin', '*');
    next();

})
var routes = require('./routes/index');
app.use('/', routes);
app.use(defaultErrorHandler)


function defaultErrorHandler(err, req, res, next) {
    res.status(500)
    res.end("something went wrong Please try again !!");
}


function init() {
   app.listen(config.serverport, function () {
        logger.log('api server listening on port ' + config.serverport);
    });
}

init()

module.exports = app;

