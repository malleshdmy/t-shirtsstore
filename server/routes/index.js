"use strict";

var express = require('express');
var router = express.Router();
var logger = require('tracer').console({
    format: "{{timestamp}} [{{title}}] {{message}} (in {{path}}:{{line}})",
    dateformat: "dd-mm-yyyy HH:MM:ss TT"
});



router.get('/', function (req, res) {
    logger.log("home")
    res.send("okay");
});


router.post('/retriveAllData', function (req, res) {

    logger.log("retriveAllData request")

    let data=[

        {   id : 1111, name : "Tie & Dye Men T-shirt", cost : 399, size : "XS", url : "assets/images/xs_1.jpeg" },
        {   id : 1112, name : "Tie & Dye Men T-shirt", cost : 299, size : "XS", url : "assets/images/xs_2.jpeg" },
        {   id : 1114, name : "Tie & Dye Men T-shirt", cost : 499, size : "XS", url : "assets/images/xs_4.jpeg" },
        {   id : 1115, name : "Tie & Dye Men T-shirt", cost : 459, size : "XS", url : "assets/images/xs_5.jpeg" },


        {   id : 2223, name : "Men V-Neck", cost : 399, size : "S", url : "assets/images/short_3.jpeg" },
        {   id : 2224, name : "Men V-Neck", cost : 499, size : "S", url : "assets/images/short_4.jpeg" },
        {   id : 2226, name : "Men V-Neck", cost : 199, size : "S", url : "assets/images/short_6.jpeg" },
        {   id : 2227, name : "Men V-Neck", cost : 299, size : "S", url : "assets/images/short_7.jpeg" },
        {   id : 2228, name : "Men V-Neck", cost : 399, size : "S", url : "assets/images/short_8.jpeg" },
        {   id : 2230, name : "Men V-Neck", cost : 599, size : "S", url : "assets/images/short_10.jpeg" },


        {   id : 3331, name : "Checkered Men Round Neck ", cost : 199, size : "M", url : "assets/images/medium_1.jpeg" },
        {   id : 3332, name : "Checkered Men Round Neck ", cost : 299, size : "M", url : "assets/images/medium_2.jpeg" },
        {   id : 3333, name : "Checkered Men Round Neck ", cost : 399, size : "M", url : "assets/images/medium_3.jpeg" },
        {   id : 3334, name : "Checkered Men Round Neck ", cost : 449, size : "M", url : "assets/images/medium_4.jpeg" },
        {   id : 3335, name : "Checkered Men Round Neck ", cost : 549, size : "M", url : "assets/images/medium_5.jpeg" },

        {   id : 4442, name : "XTie T-shirt", cost : 599, size : "L", url : "assets/images/large_2.jpeg" },
        
        {   id : 6662, name : "Checkered Men Round Neck ", cost : 899, size : "XL", url : "assets/images/xl_2.jpeg" },
        {   id : 6663, name : "Checkered Men Round Neck ", cost : 999, size : "XXL", url : "assets/images/xl_3.jpeg" },
        {   id : 6664, name : "Checkered Men Round Neck ", cost : 999, size : "XXL", url : "assets/images/xl_4.jpeg" },
        {   id : 6665, name : "Checkered Men Round Neck ", cost : 999, size : "XXL", url : "assets/images/xl_5.jpeg" }
    ]

    res.send(data);
});


module.exports = router;