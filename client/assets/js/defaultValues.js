class DefaultValues{

    constructor(){

    }
}
DefaultValues.colorCodeOne = "#495057";
DefaultValues.colorCodeTwo = "#ced4da"
DefaultValues.colorCodeThree = "#e6e6e6";
DefaultValues.colorCodeFour = "#212529";
DefaultValues.app = "App";
DefaultValues.controller = 'AppCtrl';
DefaultValues.filter = 'myFilter';
DefaultValues.sizes = ["XS","S","M","ML","L","XL","XXL"];
