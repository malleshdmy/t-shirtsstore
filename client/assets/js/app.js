var app = angular.module(DefaultValues.app, []);

app.controller(DefaultValues.controller, function ($scope,apiShirtsData) {

    let cartList = {};
    $scope.defFilterList = DefaultValues.sizes
    $scope.selecedtList = [];
    $scope.cartItemsCount = 0;
    $scope.totalAmount = 0;
    $scope.cart = [];
    $scope.cartPopUpshow = false;
    $scope.property = "cost";
    $scope.tShirts = [];

            apiShirtsData.retriveAllData().then(function (res) {

            $scope.tShirts = res.data;

            $scope.$applyAsync();

            }, function (err) {

              console.log(err)

            }).catch(function (err) {

                console.error("error")
            })
    

    $scope.allFilters = [

            {size : "XS",  id : "xtraSmall"},
            {size : "S",   id : "small"},
            {size : "M",   id : "medium"},
            {size : "ML",  id : "mediumLarge"},
            {size : "L",  id : "large"},
            {size : "XL",  id : "xtraLarge"},
            {size : "XXL",  id : "xtraXtraLarge"},
          
        ]

    $scope.tFilterList =  $scope.defFilterList;
    $scope.$applyAsync();

/* To remove an item form the cart */


    $scope.addToCart = function(shirtObj){

        if(cartList[shirtObj.id]){

            alert("Selected item is already added to the Cart");

        } else{

            $scope.cartItemsCount++;
            $scope.totalAmount += shirtObj.cost;
            $scope.cart.push(shirtObj);
            cartList[shirtObj.id] = shirtObj;
        }
    }


/* To remove an item form the cart */

    $scope.deleteItemFromCart = function(item){

            for(let data = 0; data <  $scope.cart.length; data++){

                if(item.id === $scope.cart[data].id){

                    $scope.cartItemsCount--;
                    $scope.totalAmount -= $scope.cart[data].cost;
                    $scope.cart.splice(data, 1);
                    $scope.$applyAsync();
                    delete cartList[item.id]
                    console.log(cartList)
                }
            } 
    }


    /* To order the cartdata based on criteria*/

    $scope.applyOrderby = function(){

        $scope.property = $scope.shirtCost;
    }

    /* To order the cartdata based on criteria*/


    $scope.applyFilter = function (filterObj) {

        let sizeDiv = document.getElementById(filterObj.id);

        if (filterObj.selected){

            filterObj.selected = false;

        } else {

            filterObj.selected = true;
        }

        if (filterObj.selected) {

            $scope.selecedtList.push(filterObj.size)
            $scope.tFilterList = $scope.selecedtList;
            $scope.$applyAsync();

            if(sizeDiv){

                sizeDiv.style.backgroundColor = DefaultValues.colorCodeOne;
                sizeDiv.style.color = DefaultValues.colorCodeTwo;
            }

        } else {

            for (let filter = 0; filter < $scope.selecedtList.length; filter++) {

                if ($scope.selecedtList[filter] === filterObj.size) {

                    if(sizeDiv){

                        sizeDiv.style.backgroundColor = DefaultValues.colorCodeThree
                        sizeDiv.style.color = DefaultValues.colorCodeFour;
                    }

                    $scope.selecedtList.splice(filter, 1);

                    if($scope.selecedtList <= 0){

                        $scope.tFilterList =  $scope.defFilterList;
                        $scope.$applyAsync();
                    }

                    break;
                }
            }
        }
    }

    
    $scope.showPopUp = function () {

        $scope.cartPopUpshow = true;
        $scope.$applyAsync();
    }

    $scope.closePopUP = function(){

        setTimeout(function () {

            $scope.cartPopUpshow = false;
            $scope.$applyAsync();

        },100)

    }
})


angular.module(DefaultValues.app).filter(DefaultValues.filter, function() {

    return function(inputArray, filterSizes) {

        return inputArray.filter(function (entry) {

            return this.indexOf(entry.size) !== -1;

        }, filterSizes)
    }
})
