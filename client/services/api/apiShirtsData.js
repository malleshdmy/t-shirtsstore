class ApiShirtsData {

    constructor($http) {

        this.baseUrl = "http://localhost:8080"
        
        this.C = {

            POST: "POST",
            GET: "GET",
            "Content-Type": "Content-Type",
            "application/json": "application/json"
        }


        this.$http = $http;
    }

    /* to retrive data from API Url*/
    
    retriveAllData() {

        let me = this;
        let url = me.baseUrl + "/retriveAllData"
        let headers = {};
        let req = {

            method: me.C.POST,

            url: url,

            headers: headers,

        }

        return new Promise(function (success, failure) {


            me.$http(req).then(function (res) {

                console.log("result",res)

                success(res)

            }, function (err) {

                console.log("error",err)

                failure(err)
            });
        })

        
    }
}

app.service("apiShirtsData",["$http",ApiShirtsData])
